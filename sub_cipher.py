import sys
import math
import optparse

class sub_cipher:

  def __init__(self,ciphertext_path):
    self.ciphertext_path = ciphertext_path
    self.char_counts = dict();
    self.char_mapping = dict();
    self.total_chars = 0
    self.letter_freq = [('e',12.702),('t',9.056),('a',8.167),('o',7.507),('i',6.966),('n',6.749),('s',6.327),('h',6.094),('r',5.987),('d',4.253),('l',4.025),('c',2.782),('u',2.758),('m',2.406),('w',2.360),('f',2.228),('g',2.015),('y',1.974),('p',1.929),('b',1.492),('v',0.978),('k',0.772),('j',0.153),('x',0.150),('q',0.095),('z',0.074)]

  def read_ciphertext(self):
    ciphertext = open(self.ciphertext_path)
    for word in ciphertext:
      for char in word.rstrip('\n'):
        self.char_counts[char] = self.char_counts.get(char, 0) + 1
        self.total_chars = self.total_chars + 1
    ciphertext.close()

  def print_counts(self):
    index = 0
    for char in sorted(self.char_counts, key=self.char_counts.get, reverse=True):
      print char, self.char_counts[char]/float(self.total_chars)*100.0
      self.char_mapping[char] = self.letter_freq[index][0]
      print char + "(" + str(self.letter_freq[index][1]) + ") -> " + self.letter_freq[index][0]
      index = index + 1

  def print_mapping(self):
    for char in self.char_mapping:
      print char, self.char_mapping[char]

  def decode(self): 
    ciphertext = open(self.ciphertext_path)
    for word in ciphertext:
      for char in word.rstrip('\n'):
        sys.stdout.write(self.char_mapping[char])
  
  #switch from mapping char1->a char2->b
  #to mapping char1->b char2->a
  def switch_mapping(self, char1, char2):
    prev = self.char_mapping[char1]
    self.char_mapping[char1] = self.char_mapping[char2]
    self.char_mapping[char2] = prev

  #set mapping of char1->char2
  def set_mapping(self, char1, char2):
    for char in self.char_mapping:
      if self.char_mapping[char] == char2:
        self.char_mapping[char] = self.char_mapping[char1]
        self.char_mapping[char1] = char2
        return
###########################################
def main(args):
  #parse options
  parser = optparse.OptionParser()
  parser.add_option('-f', help='path to ciphertext', dest='ciphertext', default='cipher.txt')
  opts, args = parser.parse_args()
  
  #create solver object
  solver = sub_cipher(opts.ciphertext)
  #read in ciphertext and count characters
  solver.read_ciphertext()
  #print counts
  solver.print_counts()

  #TODO: implement dictionary search
  #manual input of substitution table for this ciphertext
  solver.set_mapping('q','w')
  solver.set_mapping('b','h')
  solver.set_mapping('d','p')
  solver.set_mapping('l','o')
  solver.set_mapping('w','l')
  solver.set_mapping('v','f')
  solver.set_mapping('j','u')
  solver.set_mapping('u','i')
  solver.set_mapping('m','m')
  solver.set_mapping('c','b')
  solver.set_mapping('s','v')
  
  solver.print_mapping()
  #print result
  solver.decode()

if __name__ == "__main__":
  main(sys.argv[1:])
